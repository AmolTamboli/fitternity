//
//  StudioAPI.swift
//  fitternity
//
//  Created by Amol Tamboli on 15/07/21.
//

import Foundation

class APIHandler {
    
    static let shared = APIHandler()
    
    func getData(completion: @escaping((StudioModel) -> Void)) {
        var request = URLRequest(url: URL(string: "https://vod.fitn.in/homescreenapi/HomeScreenInStudio.json")!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
            if error == nil{
                if let data = data {
                    do {
                        let details = try? JSONDecoder().decode(StudioModel.self, from: data)
                        completion(details!)
                    } catch let err{
                        print(err.localizedDescription)
                    }
                }
            } else{
                print(error?.localizedDescription ?? "")
            }
        }
        task.resume()
    }
}
