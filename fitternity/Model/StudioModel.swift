//
//  StudioModel.swift
//  fitternity
//
//  Created by Amol Tamboli on 14/07/21.
//


import Foundation

// MARK: - StudioModel
struct StudioModel: Codable {
    let cityName: String?
    let cityID: Int?
    let instudio: String?
    let productTags: [ProductTag]?
    let campaigns: [CampaignElement]?
    let categories: Categories?
    let onepassPre: OnepassPre?
    let upcomingClasses: UpcomingClasses?
    let fitnessCentres: FitnessCentres?
    let sectionOrders: [String]?

    enum CodingKeys: String, CodingKey {
        case cityName = "city_name"
        case cityID = "city_id"
        case instudio
        case productTags = "product_tags"
        case campaigns, categories
        case onepassPre = "onepass_pre"
        case upcomingClasses = "upcoming_classes"
        case fitnessCentres = "fitness_centres"
        case sectionOrders = "section_orders"
    }
}

// MARK: - CampaignElement
struct CampaignElement: Codable {
    let image: String?
    let link: String?
    let title: String?
    let height, width: Int?
    let ratio: Double?
    let order: Int?
}

// MARK: - Categories
struct Categories: Codable {
    let title, text: String?
    let maxCategory: Int?
    let allCategoryTitle: String?
    let categorytags: [Categorytag]?
    let campaign: CategoriesCampaign?

    enum CodingKeys: String, CodingKey {
        case title, text
        case maxCategory = "max_category"
        case allCategoryTitle = "all_category_title"
        case categorytags, campaign
    }
}

// MARK: - CategoriesCampaign
struct CategoriesCampaign: Codable {
    let image: String?
    let title, bgColor, textColor: String?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case image, title
        case bgColor = "bg_color"
        case textColor = "text_color"
        case url
    }
}

// MARK: - Categorytag
struct Categorytag: Codable {
    let name, slug: String?
    let id: Int?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case name, slug
        case id = "_id"
        case image
    }
}

// MARK: - FitnessCentres
struct FitnessCentres: Codable {
    let title, fitnessCentresDescription, buttonText: String?
    let data: [FitnessCentresDatum]?

    enum CodingKeys: String, CodingKey {
        case title
        case fitnessCentresDescription = "description"
        case buttonText = "button_text"
        case data
    }
}

// MARK: - FitnessCentresDatum
struct FitnessCentresDatum: Codable {
    let averageRating: Double?
    let coverimage: String?
    let location, slug: String?
    let id: Int?
    let categorytags: [String]?
    let category: String?
    let totalRatingCount: Int?
    let flags: DatumFlagsClass?
    let commercial: String?
    let featured: Bool?
    let offeringImages: [OfferingImage]?
    let trialHeader, membershipHeader: String?
    let membershipIcon: String?
    let membershipOfferDefault: Bool?
    let membershipOffer, type, address, title: String?
    let subcategories: [String]?
    let tagImage: String?
    let abwVendor: AbwVendorUnion?

    enum CodingKeys: String, CodingKey {
        case averageRating = "average_rating"
        case coverimage, location, slug, id, categorytags, category
        case totalRatingCount = "total_rating_count"
        case flags, commercial, featured
        case offeringImages = "offering_images"
        case trialHeader = "trial_header"
        case membershipHeader = "membership_header"
        case membershipIcon = "membership_icon"
        case membershipOfferDefault = "membership_offer_default"
        case membershipOffer = "membership_offer"
        case type, address, title, subcategories
        case tagImage = "tag_image"
        case abwVendor = "abw_vendor"
    }
}

enum AbwVendorUnion: Codable {
    case abwVendorClass(AbwVendorClass)
    case anythingArray([JSONAny])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([JSONAny].self) {
            self = .anythingArray(x)
            return
        }
        if let x = try? container.decode(AbwVendorClass.self) {
            self = .abwVendorClass(x)
            return
        }
        throw DecodingError.typeMismatch(AbwVendorUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for AbwVendorUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .abwVendorClass(let x):
            try container.encode(x)
        case .anythingArray(let x):
            try container.encode(x)
        }
    }
}

// MARK: - AbwVendorClass
struct AbwVendorClass: Codable {
    let physical, online: String?
}

// MARK: - DatumFlagsClass
struct DatumFlagsClass: Codable {
    let trial, membership: String?
    let topSelling, newlyLaunched, openingSoon, comingOnOnepass: Bool?
    let state: String?
    let featured, april5, notAvailableOnOnepass, forcedOnOnepass: Bool?
    let liteClasspassAvailable: Bool?
    let covidState: String?
    let covidStateID: Int?
    let covidStateImmediately: Bool?
    let monsoonFlashDiscount: String?
    let monsoonFlashDiscountPer: Int?
    let monsoonFlashDiscountDisabled: Bool?
    let hyperLocalList: [String]?

    enum CodingKeys: String, CodingKey {
        case trial, membership
        case topSelling = "top_selling"
        case newlyLaunched = "newly_launched"
        case openingSoon = "opening_soon"
        case comingOnOnepass = "coming_on_onepass"
        case state, featured, april5
        case notAvailableOnOnepass = "not_available_on_onepass"
        case forcedOnOnepass = "forced_on_onepass"
        case liteClasspassAvailable = "lite_classpass_available"
        case covidState = "covid_state"
        case covidStateID = "covid_state_id"
        case covidStateImmediately = "covid_state_immediately"
        case monsoonFlashDiscount = "monsoon_flash_discount"
        case monsoonFlashDiscountPer = "monsoon_flash_discount_per"
        case monsoonFlashDiscountDisabled = "monsoon_flash_discount_disabled"
        case hyperLocalList = "hyper_local_list"
    }
}

// MARK: - OfferingImage
struct OfferingImage: Codable {
    let image: String?
    let height, width: Int?
}

// MARK: - OnepassPre
struct OnepassPre: Codable {
    let headerImg: String?
    let buttonText: String?
    let passes: Passes?
    let campaign: CategoriesCampaign?

    enum CodingKeys: String, CodingKey {
        case headerImg = "header_img"
        case buttonText = "button_text"
        case passes, campaign
    }
}

// MARK: - Passes
struct Passes: Codable {
    let image: String?
    let header1, header1Color, header2, subtitle: String?
    let header2Color, subheader, descHeader, onepassType: String?
    let passesDescription: String?

    enum CodingKeys: String, CodingKey {
        case image, header1
        case header1Color = "header1_color"
        case header2, subtitle
        case header2Color = "header2_color"
        case subheader
        case descHeader = "desc_header"
        case onepassType = "onepass_type"
        case passesDescription = "description"
    }
}

// MARK: - ProductTag
struct ProductTag: Codable {
    let image: String?
    let title, text: String?
    let link: String?
}

// MARK: - UpcomingClasses
struct UpcomingClasses: Codable {
    let title, upcomingClassesDescription, buttonText: String?
    let campaign: CategoriesCampaign?
    let sessionType: String?
    let data: [UpcomingClassesDatum]?

    enum CodingKeys: String, CodingKey {
        case title
        case upcomingClassesDescription = "description"
        case buttonText = "button_text"
        case campaign
        case sessionType = "session_type"
        case data
    }
}

// MARK: - UpcomingClassesDatum
struct UpcomingClassesDatum: Codable {
    let averageRating: Double?
    let name, slug, vendorSlug, vendorName: String?
    let coverimage: String?
    let overlayimage, totalSlots, nextSlot: String?
    let totalRatingCount: Int?
    let location: String?
    let address: Address?
    let specialPrice: Int?
    let campaignText, commercial, serviceType: String?
    let id, bookingPoints, sessionTime: Int?
    let overlayimageV2: Overlay?
    let ppsOneliner: String?
    let flags: FlagsUnion?
    let overlayDetails: [Overlay]?
    let tagImage: String?

    enum CodingKeys: String, CodingKey {
        case averageRating = "average_rating"
        case name, slug
        case vendorSlug = "vendor_slug"
        case vendorName = "vendor_name"
        case coverimage, overlayimage
        case totalSlots = "total_slots"
        case nextSlot = "next_slot"
        case totalRatingCount = "total_rating_count"
        case location, address
        case specialPrice = "special_price"
        case campaignText = "campaign_text"
        case commercial
        case serviceType = "service_type"
        case id
        case bookingPoints = "booking_points"
        case sessionTime = "session_time"
        case overlayimageV2 = "overlayimage_v2"
        case ppsOneliner = "pps_oneliner"
        case flags
        case overlayDetails = "overlay_details"
        case tagImage = "tag_image"
    }
}

// MARK: - Address
struct Address: Codable {
    let pincode: Int?
    let location, line3, landmark, line2: String?
    let line1: String?
}

enum FlagsUnion: Codable {
    case anythingArray([JSONAny])
    case flagsFlags(FlagsFlags)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([JSONAny].self) {
            self = .anythingArray(x)
            return
        }
        if let x = try? container.decode(FlagsFlags.self) {
            self = .flagsFlags(x)
            return
        }
        throw DecodingError.typeMismatch(FlagsUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for FlagsUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .anythingArray(let x):
            try container.encode(x)
        case .flagsFlags(let x):
            try container.encode(x)
        }
    }
}

// MARK: - FlagsFlags
struct FlagsFlags: Codable {
    let onepassMaxBookingCount: Int?

    enum CodingKeys: String, CodingKey {
        case onepassMaxBookingCount = "onepass_max_booking_count"
    }
}

// MARK: - Overlay
struct Overlay: Codable {
    let icon: String?
    let text: String?
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}

