//
//  TableViewCell.swift
//  fitternity
//
//  Created by Amol Tamboli on 14/07/21.
//

import UIKit

class TableViewCell: UITableViewCell {

    // MARK:- 1 Studio Cell
    @IBOutlet weak var studioBannerCollectionView: UICollectionView!
    @IBOutlet weak var discoverCollectionView: UICollectionView!
    @IBOutlet weak var lblDiscover: UILabel!
    @IBOutlet weak var lblDiscoverInfo: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var imgDiscount: UIImageView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var lblDiscount: UILabel!
    
    // MARK:- 2 Studio Cell
    @IBOutlet weak var imagePass: UIImageView!
    @IBOutlet weak var btnKnowMore: UIButton!
    @IBOutlet weak var imgPass: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblRed: UILabel!
    @IBOutlet weak var passImage: UIImageView!
    @IBOutlet weak var lblRedColor: UILabel!
    @IBOutlet weak var lblSubheader: UILabel!
    @IBOutlet weak var lblDesc_header: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgCompaign: UIImageView!
    @IBOutlet weak var lblCompaignTitle: UILabel!
    
    //MARK:- 3 Studio Cell
    @IBOutlet weak var upcomingCollectionView: UICollectionView!
    @IBOutlet weak var lblUpcomingDesc: UILabel!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var lblUpcomingTitle: UILabel!
    
    @IBOutlet weak var upComingfooterView: UIView!
    @IBOutlet weak var imgFooter: UIImageView!
    @IBOutlet weak var lblFooter: UILabel!
    
    //MARK:- 4 Studio Cell
    @IBOutlet weak var fitnessCollectionView: UICollectionView!
    
    
    // MARK:- Home Cell
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
