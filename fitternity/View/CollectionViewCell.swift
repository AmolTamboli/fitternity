//
//  CollectionViewCell.swift
//  fitternity
//
//  Created by Amol Tamboli on 14/07/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    //MARK:-  Header Collection View
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    
    // MARK:- Banner Studio Cell
    @IBOutlet weak var imgBannerStudio: UIImageView!
    
    // MARK:- Discover Studio Cell
    @IBOutlet weak var imgDiscover: UIImageView!
    @IBOutlet weak var lblDiscover: UILabel!
    
    //MARK:- Upcoming
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var viewRound: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblOpenTill: UILabel!
    @IBOutlet weak var imgTagImage: UIImageView!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblAvgRating: UILabel!
    @IBOutlet weak var viewRating: UIView!
    
    //MARK:- Fitness
    @IBOutlet weak var imgBg1: UIImageView!
    @IBOutlet weak var lblVendorName1: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var imgTagImage1: UIImageView!
    @IBOutlet weak var lblReview1: UILabel!
    @IBOutlet weak var lblAvgRating1: UILabel!
    @IBOutlet weak var viewRating1: UIView!
}
