//
//  ViewController.swift
//  fitternity
//
//  Created by Amol Tamboli on 13/07/21.
//

import UIKit
import Foundation
import Kingfisher
import ListPlaceholder

class ViewController: UIViewController {
    
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var headerCollectionView: UICollectionView!
    @IBOutlet weak var btlLocation: UIButton!
    @IBOutlet weak var btnDiscount: UIButton!
    
    var resultArr = [StudioModel]()
    
    var studioBannerCollectionView: UICollectionView!
    var discoverCollectionView: UICollectionView!
    var upcomingCollectionView: UICollectionView!
    var fitnessCollectionView: UICollectionView!
    
    let btnStudio = UIButton()
    let btnHome = UIButton()
    let viewStudio = UIView()
    let viewHome = UIView()
    
    var type: String = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.getStudioData()
    }
    
    func getStudioData() {
        self.view.showLoader()
        APIHandler.shared.getData { response in
            self.resultArr = [response]
            print(self.resultArr.count)
            DispatchQueue.main.async {
                self.view.hideLoader()
                self.btlLocation.setTitle(self.resultArr[0].cityName ?? "", for: .normal)
                let imgUrl = self.resultArr[0].categories?.campaign?.image ?? ""
                let url = URL(string: imgUrl)
                self.btnDiscount.kf.setBackgroundImage(with: url, for: .normal)
                self.headerCollectionView.reloadData()
                self.tblHome.reloadData()
            }
            
            // Call New Api
            
            //            APIHandler.shared.getData { response in
            //                self.resultArr = [response]
            //            }
            
        }
    }
    
    func setupUI() {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        
        btnStudio.addTarget(self, action: #selector(studioCLicked(sender:)), for: .touchUpInside)
        btnHome.addTarget(self, action: #selector(homeClicked(sender:)), for: .touchUpInside)
        
        btnStudio.setTitleColor(#colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1), for: .normal)
        viewStudio.backgroundColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1)
        
        btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnHome.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewHome.backgroundColor = .clear
        tblHome.reloadData()
    }
    
    @objc func studioCLicked(sender: UIButton){
        self.type = "1"
        btnStudio.setTitleColor(#colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1), for: .normal)
        viewStudio.backgroundColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1)
        
        btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnHome.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewHome.backgroundColor = .clear
        tblHome.reloadData()
    }
    
    @objc func homeClicked(sender: UIButton){
        self.type = "2"
        btnHome.setTitleColor(#colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1), for: .normal)
        viewHome.backgroundColor = #colorLiteral(red: 1, green: 0.5960784314, blue: 0.1137254902, alpha: 1)
        
        btnStudio.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnStudio.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewStudio.backgroundColor = .clear
        tblHome.reloadData()
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == "1" {
            if resultArr.count > 0 {
                return 4
            } else {
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == "1"{
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "bannerTableViewCell", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                
                studioBannerCollectionView = cell.studioBannerCollectionView
                cell.studioBannerCollectionView.delegate = self
                cell.studioBannerCollectionView.dataSource = self
                cell.studioBannerCollectionView.reloadData()
                
                discoverCollectionView = cell.discoverCollectionView
                cell.discoverCollectionView.delegate = self
                cell.discoverCollectionView.dataSource = self
                cell.discoverCollectionView.reloadData()
                
                cell.lblDiscover.text = resultArr[0].categories?.title ?? ""
                cell.lblDiscoverInfo.text = resultArr[0].categories?.text ?? ""
                
                cell.lblDiscount.text = resultArr[0].categories?.campaign?.title ?? ""
                let textcolor = resultArr[0].categories?.campaign?.textColor ?? ""
                cell.lblDiscount.textColor = hexStringToUIColor(hex: textcolor)
                
                let imgUrl = resultArr[0].categories?.campaign?.image ?? ""
                let url = URL(string: imgUrl)
                cell.imgDiscount.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"))
                
                let color = resultArr[0].categories?.campaign?.bgColor ?? ""
                cell.discountView.layer.cornerRadius = 8
                cell.discountView.backgroundColor = hexStringToUIColor(hex: color)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailsTableViewCell", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                
                let btnTitle = resultArr[0].onepassPre?.buttonText ?? ""
                
                cell.btnKnowMore.setTitle(btnTitle, for: .normal)
                
                let info = resultArr[0].onepassPre?.passes
                cell.lblDesc.text = info?.passesDescription ?? ""
                cell.lblSubtitle.text = info?.subtitle ?? ""
                
                let imageUrl = info?.image ?? ""
                let urlImg = URL(string: imageUrl)
                cell.imgPass.kf.setImage(with: urlImg, placeholder: UIImage(named: "img_Placeholder"))
                cell.imgPass.layer.cornerRadius = 10
                cell.imgPass.layer.masksToBounds = true
                
                let iconUrl = resultArr[0].onepassPre?.headerImg ?? ""
                let ImgIcon = URL(string: iconUrl)
                cell.imagePass.kf.setImage(with: ImgIcon, placeholder: UIImage(named: "img_Placeholder"))
                cell.passImage.kf.setImage(with: ImgIcon, placeholder: UIImage(named: "img_Placeholder"))
                cell.passImage.image = cell.passImage.image?.withRenderingMode(.alwaysTemplate)
                cell.passImage.tintColor = UIColor.black
                
                cell.lblRed.text = info?.header2 ?? ""
                cell.lblRedColor.text = info?.header2 ?? ""
                let color = info?.header2Color ?? ""
                cell.lblRed.textColor = hexStringToUIColor(hex: color)
                cell.lblRedColor.textColor = hexStringToUIColor(hex: color)
                cell.lblSubheader.text = info?.subheader ?? ""
                cell.lblDesc_header.text = info?.descHeader ?? ""
                
                let campaign = resultArr[0].onepassPre?.campaign
                
                cell.lblCompaignTitle.text = campaign?.title ?? ""
                let colors = campaign?.bgColor ?? ""
                cell.bgView.layer.cornerRadius = 8
                cell.bgView.backgroundColor = hexStringToUIColor(hex: colors)
                
                
                
                let imageUrls = campaign?.image ?? ""
                let compaign = URL(string: imageUrls)
                cell.imgCompaign.kf.setImage(with: compaign, placeholder: UIImage(named: "img_Placeholder"))
                
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "upComingClass", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                
                let upComingData = resultArr[0].upcomingClasses
                cell.lblUpcomingTitle.text = upComingData?.title ?? ""
                cell.lblUpcomingDesc.text = upComingData?.upcomingClassesDescription ?? ""
                
                let btnTitle = upComingData?.buttonText ?? ""
                cell.btnUpcoming.setTitle(btnTitle, for: .normal)
                
                upcomingCollectionView = cell.upcomingCollectionView
                cell.upcomingCollectionView.delegate = self
                cell.upcomingCollectionView.dataSource = self
                cell.upcomingCollectionView.reloadData()
                
                let footUpcoming = upComingData?.campaign
                
                let FooterViewColor = footUpcoming?.bgColor ?? ""
                cell.upComingfooterView.layer.cornerRadius = 8
                cell.upComingfooterView.backgroundColor = hexStringToUIColor(hex: FooterViewColor)
                
                cell.lblFooter.text = footUpcoming?.title ?? ""
                
                let imageUrls = footUpcoming?.image ?? ""
                let compaign = URL(string: imageUrls)
                cell.imgFooter.kf.setImage(with: compaign, placeholder: UIImage(named: "img_Placeholder"))
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "fitnessCentre", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                
                let upComingData = resultArr[0].fitnessCentres
                cell.lblUpcomingTitle.text = upComingData?.title ?? ""
                cell.lblUpcomingDesc.text = upComingData?.fitnessCentresDescription ?? ""
                
                let btnTitle = upComingData?.buttonText ?? ""
                cell.btnUpcoming.setTitle(btnTitle, for: .normal)
                
                fitnessCollectionView = cell.fitnessCollectionView
                cell.fitnessCollectionView.delegate = self
                cell.fitnessCollectionView.dataSource = self
                cell.fitnessCollectionView.reloadData()
                
                return cell
            }
        } else {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "fitnessCentre", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                
                let upComingData = resultArr[0].fitnessCentres
                cell.lblUpcomingTitle.text = upComingData?.title ?? ""
                cell.lblUpcomingDesc.text = upComingData?.fitnessCentresDescription ?? ""
                
                let btnTitle = upComingData?.buttonText ?? ""
                cell.btnUpcoming.setTitle(btnTitle, for: .normal)
                
                fitnessCollectionView = cell.fitnessCollectionView
                cell.fitnessCollectionView.delegate = self
                cell.fitnessCollectionView.dataSource = self
                cell.fitnessCollectionView.reloadData()
                
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        v.layer.masksToBounds = false
        v.layer.shadowRadius = 4
        v.layer.shadowOpacity = 1
        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize(width: 0 , height:2)
        
        btnStudio.setTitle("Workout In-Studio", for: .normal)
        btnStudio.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btnStudio.translatesAutoresizingMaskIntoConstraints = false
        
        btnHome.setTitle("Workout At Home", for: .normal)
        btnHome.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btnHome.translatesAutoresizingMaskIntoConstraints = false
        
        let buttonStackView = UIStackView()
        buttonStackView.frame = CGRect(x: 10*DeviceMultiplier, y: 5*DeviceMultiplier, width: tableView.frame.width - 20*DeviceMultiplier, height: 26*DeviceMultiplier)
        buttonStackView.alignment = .fill
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing = 8.0
        
        buttonStackView.addArrangedSubview(btnStudio)
        buttonStackView.addArrangedSubview(btnHome)
        v.addSubview(buttonStackView)
        
        let StackView = UIStackView()
        StackView.frame = CGRect(x: 0 , y: buttonStackView.frame.origin.y + buttonStackView.frame.height, width: ScreenWidth, height: 4*DeviceMultiplier)
        StackView.alignment = .fill
        StackView.distribution = .fillEqually
        StackView.spacing = 8.0
        viewStudio.layer.cornerRadius = 2.5
        viewHome.layer.cornerRadius = 2.5
        
        StackView.addArrangedSubview(viewStudio)
        StackView.addArrangedSubview(viewHome)
        
        v.addSubview(StackView)
        
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if type == "1"{
            if indexPath.row == 0 {
                return UITableView.automaticDimension
            } else  if indexPath.row == 1 {
                return UITableView.automaticDimension
            } else  if indexPath.row == 2 {
                return UITableView.automaticDimension
            } else  if indexPath.row == 3 {
                return UITableView.automaticDimension
            }
        }
        return 0
    }
}












extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == headerCollectionView{
            if resultArr.count > 0 {
                return resultArr[0].productTags?.count ?? 0
            } else {
                return 0
            }
        } else if collectionView == studioBannerCollectionView {
            if resultArr.count > 0 {
                return resultArr[0].campaigns?.count ?? 0
            } else {
                return 0
            }
        } else if collectionView == discoverCollectionView {
            if resultArr.count > 0 {
                return 5
            } else {
                return 0
            }
        } else if collectionView == upcomingCollectionView {
            if resultArr.count > 0 {
                return resultArr[0].upcomingClasses?.data?.count ?? 0
            } else {
                return 0
            }
        } else if collectionView == fitnessCollectionView {
            if resultArr.count > 0 {
                return resultArr[0].fitnessCentres?.data?.count ?? 0
            } else {
                return 0
            }
        }  else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == headerCollectionView {
            let cell = headerCollectionView.dequeueReusableCell(withReuseIdentifier: "headerCollectionView", for: indexPath) as! CollectionViewCell
            
            let products = resultArr[0].productTags?[indexPath.row]
            
            let imgUrl = products?.image ?? ""
            let url = URL(string: imgUrl)
            cell.img.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"))
            cell.lblTitle.text = products?.title
            cell.lblText.text = products?.text
            
            return cell
        } else if collectionView == studioBannerCollectionView {
            let cell = studioBannerCollectionView.dequeueReusableCell(withReuseIdentifier: "bannerCollectionViewCell", for: indexPath) as! CollectionViewCell
            
            let campaigns = resultArr[0].campaigns?[indexPath.row]
            
            let imgUrl = campaigns?.image ?? ""
            let url = URL(string: imgUrl)
            cell.imgBannerStudio.kf.indicatorType = .activity
            cell.imgBannerStudio.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"), options: [.transition(.fade(0.7))], progressBlock: nil, completionHandler: nil)
            
            return cell
        } else if collectionView == discoverCollectionView {
            let cell = discoverCollectionView.dequeueReusableCell(withReuseIdentifier: "discoverCollectionViewCell", for: indexPath) as! CollectionViewCell
            
            let categorytags = resultArr[0].categories?.categorytags?[indexPath.row]
            
            let imgUrl = categorytags?.image ?? ""
            let url = URL(string: imgUrl)
            cell.imgDiscover.layer.cornerRadius = 8
            cell.imgDiscover.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"))
            
            cell.lblDiscover.text = categorytags?.slug ?? ""
            
            return cell
        } else if collectionView == upcomingCollectionView {
            let cell = upcomingCollectionView.dequeueReusableCell(withReuseIdentifier: "upcomingCollectionViewCell", for: indexPath) as! CollectionViewCell
            
            let upComing = resultArr[0].upcomingClasses?.data?[indexPath.row]
            
            let imgUrlTag = upComing?.tagImage ?? ""
            let urlTag = URL(string: imgUrlTag)
            cell.imgTagImage.kf.setImage(with: urlTag, placeholder: UIImage(named: "img_Placeholder"))
            
            let imgUrl = upComing?.coverimage ?? ""
            let url = URL(string: imgUrl)
            cell.imgBg.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"))
            cell.lblVendorName.text = upComing?.vendorName ?? ""
            cell.lblAddress.text = upComing?.location ?? ""
            cell.lblOpenTill.text = upComing?.nextSlot ?? ""
            let decimal = upComing?.averageRating ?? 0
            let doubleStr = String(format: "%.2f", decimal)
            cell.lblAvgRating.text = doubleStr
            let ratingCount = upComing?.totalRatingCount ?? 0
            cell.lblReview.text = "\(ratingCount)" + "\n" + "Reviews"
            cell.viewRating.layer.cornerRadius = 3
            
            let roundData = upComing?.overlayDetails
            
            let iconUrl0 = roundData?[0].icon ?? ""
            let urlIcon0 = URL(string: iconUrl0)
            cell.img1.kf.setImage(with: urlIcon0, placeholder: UIImage(named: "img_Placeholder"))
            cell.lbl1.text = roundData?[0].text ?? ""
            
            let iconUrl = roundData?[1].icon ?? ""
            let urlIcon = URL(string: iconUrl)
            cell.img2.kf.setImage(with: urlIcon, placeholder: UIImage(named: "img_Placeholder"))
            cell.lbl2.text = roundData?[1].text ?? ""
            
            let iconUrl2 = roundData?[2].icon ?? ""
            let urlIcon2 = URL(string: iconUrl2)
            cell.img3.kf.setImage(with: urlIcon2, placeholder: UIImage(named: "img_Placeholder"))
            cell.lbl3.text = roundData?[2].text ?? ""
            
            return cell
        } else {
            let cell = fitnessCollectionView.dequeueReusableCell(withReuseIdentifier: "fitnessCollectionViewCell", for: indexPath) as! CollectionViewCell
            
            let upComing = resultArr[0].fitnessCentres?.data?[indexPath.row]
            
            let imgUrlTag = upComing?.tagImage ?? ""
            let urlTag = URL(string: imgUrlTag)
            cell.imgTagImage1.kf.setImage(with: urlTag, placeholder: UIImage(named: "img_Placeholder"))
            
            let imgUrl = upComing?.coverimage ?? ""
            let url = URL(string: imgUrl)
            cell.imgBg1.kf.setImage(with: url, placeholder: UIImage(named: "img_Placeholder"))
            
            cell.lblVendorName1.text = upComing?.slug ?? ""
            cell.lblAddress1.text = upComing?.location ?? ""
            let decimal = upComing?.averageRating ?? 0
            let doubleStr = String(format: "%.2f", decimal)
            cell.lblAvgRating1.text = doubleStr
            let ratingCount = upComing?.totalRatingCount ?? 0
            cell.lblReview1.text = "\(ratingCount)" + "\n" + "Reviews"
            cell.viewRating1.layer.cornerRadius = 3
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == headerCollectionView {
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont(name: "Avenir Next", size: 14)
            label.text = resultArr[0].productTags?[indexPath.row].text ?? ""
            label.sizeToFit()
            return CGSize(width:label.frame.width + 90, height:55)
        } else if collectionView == studioBannerCollectionView{
            return CGSize(width:collectionView.frame.size.width, height: 150)
        } else if collectionView == discoverCollectionView{
            return CGSize(width:collectionView.bounds.width/2.7, height: 160)
        } else if collectionView == upcomingCollectionView{
            return CGSize(width: collectionView.frame.size.width - (20*DeviceMultiplier), height: 240)
        }  else if collectionView == fitnessCollectionView{
            return CGSize(width: collectionView.frame.size.width - (60*DeviceMultiplier), height: 240)
        }
        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == studioBannerCollectionView {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == studioBannerCollectionView{
            return 10
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == headerCollectionView {
            return 10
        } else {
            return 10
        }
        
    }
    
    
    
}


